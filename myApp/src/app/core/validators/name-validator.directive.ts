import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[appNameValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: NameValidatorDirective, multi: true }]
})
export class NameValidatorDirective {
validate(control: AbstractControl): ValidationErrors | null {
  if (!control.value) { return null };

  for (let i=0; i<control.value.length; i++) {
    if ( !isNaN(parseInt(control.value[i])) ) {
      return { nameContainsNums: ['Name must contain only letters!'] };
    }
  }

  if (control.value.length < 3) {
    return { nameTooShort: ['Name must be at least 3 letters long!'] }
}
  return null;
  }
}
