import { Directive } from '@angular/core';
import { FormGroup, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[passwordsComparer]',
    providers: [{ provide: NG_VALIDATORS, useExisting: PasswordsValidator, multi: true }]
})
export class PasswordsValidator {

    static compare(form: FormGroup): ValidationErrors | null {
        const password = form.get('password');
        const rePass = form.get('rePass');
        if (password?.pristine || rePass?.pristine) return null;
        if (password?.value != rePass?.value) return { comparisonError: true };
        return null;
    }
}