import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { AuthService } from "../../auth/service/auth.service";


@Injectable()
export class LandingPageGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
        private router: Router
    ) { }
    canActivate() {
        if ( this.authService.getUser() == null ) return true;
        else return this.router.navigate([ "/employees" ]);
    }
}