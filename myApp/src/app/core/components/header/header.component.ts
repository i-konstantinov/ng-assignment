import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

import { AuthService } from 'src/app/auth/service/auth.service';
import { EmployeeModalComponent } from 'src/app/employees/components/employee-modal/employee-modal.component';
import { Employee } from 'src/app/employees/models/employee';

import { selectUser, selectUsersEmployeeProfile } from 'src/app/auth/auth.state';
import { User } from 'src/app/auth/models/userDto';

import * as Actions from "./actions/header.component.actions";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUser$!: (Observable<User | null>) | null;
  employeeProfile$!: (Observable<Employee | null>) | null;

  constructor(
    public readonly translator: TranslateService,
    private readonly authService: AuthService,
    private dialog: MatDialog,
    private store: Store
  ) {
    this.currentUser$ = this.store.select(selectUser);
    this.employeeProfile$ = this.store.select(selectUsersEmployeeProfile);
  }

  ngOnInit(): void {
    const user = this.authService.getUser();
    if (user) {
      this.store.dispatch(Actions.getProfile({
        user: {
          username: user.username,
          id: user.id
        }
      }));
    }
    this.translatorConfig();
  }

  onAllEmployees(): void {
    this.store.dispatch(Actions.goToEmployeesList({ filters:{ page: 1, limit: 10 }}));
  }


  onViewProfile(profile: Employee): void {
    this.store.dispatch(Actions.viewProfile({ employee: profile }));
    const detailsDialogConfig = new MatDialogConfig();
    detailsDialogConfig.disableClose = true;
    this.dialog.open(EmployeeModalComponent, detailsDialogConfig);
  }

  onLogout(): void {
    this.authService.logout();
    this.store.dispatch(Actions.logOut());
  }

  translatorConfig(): void {
    this.translator.addLangs(['en', 'bg']);
    this.translator.setDefaultLang('en');
    const browserLang = this.translator.getBrowserLang();
    this.translator.use(browserLang?.match(/en|bg/) ? browserLang : 'en');
  }
}