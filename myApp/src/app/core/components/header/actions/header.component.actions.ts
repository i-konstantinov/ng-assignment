import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/auth/models/userDto';
import { Employee } from 'src/app/employees/models/employee';
import { FiltersDto } from 'src/app/employees/models/filtersDto';


export const getProfile = createAction(
    "[Header Component] Get User's Employee Profile",
    props<{ user: User }>()
);

export const goToEmployeesList = createAction(
    "[Header Component] Go to Employees List",
    props<{ filters: FiltersDto }>()
);
export const addProfile = createAction(
    "[Header Component] Go to Add Employee"
);
export const viewProfile = createAction(
    "[Header Component] Go to View Profile",
    props<{ employee: Employee }>()
);
export const logOut = createAction(
    "[Header Component] Log Out"
);