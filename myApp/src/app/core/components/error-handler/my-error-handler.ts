import { HttpErrorResponse } from "@angular/common/http";
import { ErrorHandler, Injectable } from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { ErrorHandlerComponent } from "./error-handler.component";


@Injectable()
export class MyErrorHandler implements ErrorHandler {
  constructor(
    private dialog: MatDialog
  ) { }

  handleError(err: HttpErrorResponse) {
    const configuration = new MatDialogConfig();
    configuration.width = "fit-content";
    configuration.height = "fit-content";

    if (err.error) {
      configuration.data = err.error;
    }
    else {
      configuration.data = { statusCode: err.status, message: err.message };
    }
    this.dialog.open(ErrorHandlerComponent, configuration);
  }
}