import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-error-handler',
  templateUrl: './error-handler.component.html',
  styleUrls: ['./error-handler.component.css']
})
export class ErrorHandlerComponent{
  constructor(
    private dialog: MatDialogRef<ErrorHandlerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onClose() {
    this.dialog.close();
  }
}