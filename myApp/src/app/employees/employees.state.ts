import { NgModule } from "@angular/core";
import { ActionReducerMap, createFeatureSelector, createSelector, MetaReducer, StoreModule } from "@ngrx/store";

import * as employeesReducer from "./employees.reducer";

export const FEATURE_KEY = "employees";


/**
 * The shape of the State
 */
export interface State {
    employees: employeesReducer.State
};

export const reducers: ActionReducerMap<State> = {
    employees: employeesReducer.reducer
};

export const metaReducers: MetaReducer<State>[] = [];

/**
 * Module
 */
@NgModule({
    imports: [StoreModule.forFeature(FEATURE_KEY, reducers, { metaReducers })],
})
export class EmployeesStateModule {}


/**
 * Feature Selector
 */
export const selectEmployeesFeatureState = createFeatureSelector<State>(FEATURE_KEY);


/**
 * Employees Selectors
 */
export const selectEmployeesState = createSelector(
    selectEmployeesFeatureState,
    (stateData) => stateData.employees
);

export const selectAllEmployees = createSelector(
    selectEmployeesState,
    employeesReducer.selectAllEmployees
);

export const selectTotalEmployees = createSelector(
    selectEmployeesState,
    employeesReducer.selectTotalEmployees
);

export const selectActiveEmployee = createSelector(
    selectEmployeesState,
    employeesReducer.selectActiveEmployee
);

export const selectPaginator = createSelector(
    selectEmployeesState,
    employeesReducer.selectPaginatorData
);

export const selectEmployeesCities = createSelector(
    selectEmployeesState,
    employeesReducer.selectCities
);

export const selectEmployeesDepartments = createSelector(
    selectEmployeesState,
    employeesReducer.selectDepartments
);