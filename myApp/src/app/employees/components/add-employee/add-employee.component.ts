import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { NameValidatorDirective } from 'src/app/core/validators/name-validator.directive';
import { Store } from '@ngrx/store';

import * as AddEmployeePageActions from "./actions/add-employee-page.actions";
import { selectUserId } from 'src/app/auth/auth.state';
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  form: any;
  userId!: number | undefined;
  idSubscription!: Subscription;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private store: Store
  ) {
    this.form = this.fb.group({
      firstName: ['', [Validators.required, new NameValidatorDirective]],
      lastName: ['', [Validators.required, new NameValidatorDirective]],
      email: ['', [Validators.required, Validators.email]],
      position: ['', Validators.required],
      salary: ['', [Validators.required, Validators.min(0)]],
      department: ['', Validators.required],
      city: ['', [Validators.required, new NameValidatorDirective]]
    });
  }


  ngOnInit(): void {
    this.store.dispatch(AddEmployeePageActions.enter());
    this.idSubscription = this.store.select(selectUserId).subscribe(
      id => this.userId = id
    );
  }


  addEmployeeFromForm(): void {
    if (this.form.invalid) { return }
    if (this.userId === undefined) { throw new Error("User is undefined"); }

    const data = this.form.value;
    data.userId = Number(this.userId);

    this.store.dispatch(AddEmployeePageActions.submitForm({
      newEmployee: data
    }));

    this.idSubscription.unsubscribe();
    this.router.navigate(["/employees"]);
  }
}
