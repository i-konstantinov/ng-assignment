import { createAction, props } from "@ngrx/store";
import { Employee } from "src/app/employees/models/employee";


export const employeeAdded = createAction(
    "[Add Employee] Employee Added",
    props<{ employee:  Employee }>()
);

export const employeeAddedFail = createAction(
    "[Add Employee] Employee Not Added"
)