import { createAction, props } from "@ngrx/store";
import { EmployeeDto } from "src/app/employees/models/employeeDto";


export const enter = createAction("[Add Employee] Enter");

export const submitForm =  createAction(
    "[Add Employee] Submit",
    props<{ newEmployee: EmployeeDto }>()
);