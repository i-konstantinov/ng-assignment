import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, concatMap } from "rxjs";
import { EmployeesService } from "src/app/employees/employees.service";

import * as AddEmployeeApiActions from "../actions/add-employee-api.actions";
import * as AddEmployeePageActions from "../actions/add-employee-page.actions";


@Injectable()
export class AddEmployeeApiEffects {
    constructor(
        private employeesService: EmployeesService,
        private actions: Actions
    ) { }
    
    addEmployee$ = createEffect(() => {
        return this.actions.pipe(
            ofType(AddEmployeePageActions.submitForm),
            concatMap((data) => {
                return this.employeesService.addEmployee(data.newEmployee)
                .pipe(map((response) => AddEmployeeApiActions.employeeAdded({ employee: response })))
            })
        )
    });
}