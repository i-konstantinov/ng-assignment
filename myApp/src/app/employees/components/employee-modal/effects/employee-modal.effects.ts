import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, concatMap, mergeMap } from "rxjs";
import { EmployeesService } from "src/app/employees/employees.service";

import * as PageActions from "../actions/employee-modal-page.actions";
import * as ApiActions from "../actions/employee-modal-api.actions";


@Injectable()
export class EmployeeModalEffects {
    constructor(
        private employeesService: EmployeesService,
        private actions: Actions
    ) { }

    updateEmployee$ = createEffect(() => {
        return this.actions.pipe(
            ofType(PageActions.submitForm),
            concatMap((data) => {
                return this.employeesService.updateEmployee(data.employeeProps, data.employeeId)
                .pipe(map((response) => ApiActions.employeeUpdated({ employee: response })))
            })
        )
    });

    deleteEmployee$ = createEffect(() => {
        return this.actions.pipe(
            ofType(PageActions.deleteEmployee),
            mergeMap(({ employeeId }) => {
                return this.employeesService.deleteEmployee(employeeId)
                .pipe(map(() => ApiActions.employeeDeleted({ employeeId })))
            })
        )
    });
}
