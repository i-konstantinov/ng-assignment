import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { NameValidatorDirective } from 'src/app/core/validators/name-validator.directive';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { User } from 'src/app/auth/models/userDto';
import { Employee } from '../../models/employee';
import { selectActiveEmployee } from '../../employees.state';
import { selectUser } from 'src/app/auth/auth.state';

import * as PageActions from "./actions/employee-modal-page.actions";


@Component({
  selector: 'app-employee-details-modal',
  templateUrl: './employee-modal.component.html',
  styleUrls: ['./employee-modal.component.css']
})
export class EmployeeModalComponent implements OnInit, OnDestroy {
  updateForm: any;

  employeeData!: Employee | null;
  currentUser!: User | null;

  detailsView: boolean = true;
  updateView: boolean = false;

  userSubscription!: Subscription;
  employeeSubscription!: Subscription;

  constructor(
    private store: Store,
    private formBuilder: FormBuilder,
    private dialog: MatDialogRef<EmployeeModalComponent>
  ) {
    this.userSubscription = this.store.select(selectUser).subscribe(user => this.currentUser = user);

    this.employeeSubscription = this.store.select(selectActiveEmployee).subscribe(data => this.employeeData = data);
  }

  ngOnInit(): void {
    this.store.dispatch(PageActions.enter());
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
    this.employeeSubscription.unsubscribe();
  }

  onCloseModal(): void {
    this.store.dispatch(PageActions.exit());
    this.dialog.close()
  }


  onUpdateView(): void {
    this.updateView = true;
    this.detailsView = false;

    this.updateForm = this.formBuilder.group({
      firstName: [this.employeeData?.firstName, [Validators.required, new NameValidatorDirective]],
      lastName: [this.employeeData?.lastName, [Validators.required, new NameValidatorDirective]],
      email: [this.employeeData?.email, [Validators.required, Validators.email]],
      position: [this.employeeData?.position, Validators.required],
      salary: [this.employeeData?.salary, [Validators.required, Validators.min(0)]],
      department: [this.employeeData?.department, Validators.required],
      city: [this.employeeData?.city, [Validators.required, new NameValidatorDirective]],
      status: [this.employeeData?.status]
    });
  }

  onSubmit(): void {
    if (this.employeeData!.userId != this.currentUser?.id) { return };
    if (this.updateForm.invalid) { return };

    const data = this.updateForm.value;
    data.userId = Number(this.currentUser.id);

    this.store.dispatch(PageActions.submitForm({ employeeProps: data, employeeId: this.employeeData!.id }));

    this.updateView = false;
    this.detailsView = true;
  }

  onCancelUpdate(): void {
    this.updateView = false;
    this.detailsView = true;
  }


  onDelete(): void {
    if (this.employeeData!.userId != this.currentUser?.id) { return };

    if (confirm("ARE YOU SURE?")) {
      this.store.dispatch(PageActions.deleteEmployee({ employeeId: this.employeeData!.id }));
      this.dialog.close();
    }
  }
}