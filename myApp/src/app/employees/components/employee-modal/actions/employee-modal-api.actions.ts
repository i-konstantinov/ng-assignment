import { createAction, props } from '@ngrx/store';
import { Employee } from 'src/app/employees/models/employee';


export const employeeUpdated = createAction(
    "[Employee Modal] Employee Updated",
    props<{ employee: Employee }>()
);
export const employeeUpdatedFail = createAction("[Employee Modal] Update Failed");

export const employeeDeleted = createAction(
    "[Employee Modal] Employee Deleted",
    props<{ employeeId: number }>()
);

export const employeeDeletedFail = createAction("[Employee Modal] Delete Failed");
