import { createAction, props } from '@ngrx/store';
import { EmployeeDto } from 'src/app/employees/models/employeeDto';


export const enter = createAction(
    "[Employee Modal] Enter"
);

export const submitForm = createAction(
    "[Employee Modal] Update Submit",
    props<{ employeeProps: EmployeeDto, employeeId: number }>()
);


export const deleteEmployee = createAction(
    "[Employee Modal] Delete Employee",
    props<{ employeeId: number }>()
);

export const exit = createAction(
    "[Employee Modal] Exit"
);