import { createAction, props } from '@ngrx/store';
import { Employee } from 'src/app/employees/models/employee';
import { FiltersDto } from 'src/app/employees/models/filtersDto';


export const enter = createAction(
    "[Employees List] Enter",
    props<{ filters: FiltersDto }>()
);

export const loadDepartments = createAction(
    "[Employees Filter] Load Departments"
);

export const loadCities = createAction(
    "[Employees Filter] Load Cities"
);

export const openDetails = createAction(
    "[Employees List] Open Employee Modal",
    props<{ employee: Employee }>()
);

export const tableUpdate = createAction(
    "[Employees Filter] Filters Applied",
    props<{ filters: FiltersDto }>()
);

export const pageChanged = createAction(
    "[Employees List] Table Page Changed",
    props<{ filters: FiltersDto }>()
);