import { createAction, props } from '@ngrx/store';
import { FilteredEmployees } from 'src/app/employees/models/filteredEmployees';



export const departmentsLoaded = createAction(
    "[Employees Filter] Departments Loaded",
    props<{ departments: string[] }>()
); 

export const citiesLoaded = createAction(
    "[Employees Filter] Cities Loaded",
    props<{ cities: string[] }>()
);

export const filteredEmployeesLoaded = createAction(
    "[Employees List] Employees Loaded Success",
    props<{ filteredData: FilteredEmployees }>()
);

export const employeesLoadedFail = createAction("[Employees List] Employees Loaded Fail");

export const totalNumberLoaded = createAction(
    "[Employees List] Total Loaded Success",
    props<{ total: number }>()
);
