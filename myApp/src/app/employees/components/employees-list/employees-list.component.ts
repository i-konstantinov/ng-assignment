import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Employee } from 'src/app/employees/models/employee';
import { PaginatorMetaData } from '../../models/paginatorMetaData';
import { EmployeeModalComponent } from '../employee-modal/employee-modal.component';
import { selectAllEmployees, selectEmployeesCities, selectEmployeesDepartments, selectPaginator, selectTotalEmployees } from '../../employees.state';

import * as PageActions from './actions/employees-list-page.actions';


@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {

  departments$!: Observable<string[]>;
  cities$!: Observable<string[]>;
  totalEmployees$!: Observable<number | undefined>;
  employeesRecords$!: Observable<Employee[]>;
  paginatorData$!: Observable<PaginatorMetaData | null>;
  displayedColumns: string[] = ['name', 'position', 'department', 'city', "details"];
  @ViewChildren(MatSelect) selectors!: QueryList<MatSelect>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;


  constructor(
    private dialog: MatDialog,
    private store: Store
  ) {
    this.store.dispatch(PageActions.loadDepartments());
    this.store.dispatch(PageActions.loadCities());
  }


  ngOnInit(): void {
    this.store.dispatch(PageActions.enter({
      filters: {
        page: 1,
        limit: 10
      }
    }));
    this.departments$ = this.store.select(selectEmployeesDepartments);
    this.cities$ = this.store.select(selectEmployeesCities);
    this.employeesRecords$ = this.store.select(selectAllEmployees);    
    this.totalEmployees$ = this.store.select(selectTotalEmployees);
    this.paginatorData$ = this.store.select(selectPaginator);
  }


  openDetailsDialog(employee: Employee): void {
    this.store.dispatch(PageActions.openDetails({ employee }));

    const detailsDialogConfig = new MatDialogConfig();
    detailsDialogConfig.disableClose = true;
    detailsDialogConfig.height = "fit-content";
    detailsDialogConfig.width = "fit-content";

    this.dialog.open(EmployeeModalComponent);
  }


  updateTable() {
    this.store.dispatch(PageActions.tableUpdate({
      filters: {
        page: this.paginator.pageIndex + 1,
        limit: this.paginator.pageSize,
        department: this.selectors.first.value,
        city: this.selectors.last.value
      }
    }));
  }


  resetFilters() {
    this.selectors.first.value = null;
    this.selectors.last.value = null;

    this.store.dispatch(PageActions.tableUpdate({
      filters: {
        page: 1,
        limit: this.paginator.pageSize
      }
    }));
  }
}