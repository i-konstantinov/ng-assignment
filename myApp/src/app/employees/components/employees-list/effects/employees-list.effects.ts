import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { map, exhaustMap } from "rxjs/operators";
import { EmployeesService } from "src/app/employees/employees.service";

import * as PageActions from '../actions/employees-list-page.actions';
import * as ApiActions from '../actions/employees-list-api.actions';
import { goToEmployeesList as GoToEmployeesListFromHeader } from "../../../../core/components/header/actions/header.component.actions";


@Injectable()
export class EmployeesListEffects {
    constructor(
        private employeesService: EmployeesService,
        private actions: Actions
    ) { }


    loadFilteredEmployees$ = createEffect(() => {
        return this.actions.pipe(
            ofType(
                PageActions.enter,
                PageActions.tableUpdate,
                GoToEmployeesListFromHeader
            ),
            exhaustMap((data) => {
                return this.employeesService.loadFilteredEmployees(data.filters)
                    .pipe(map(data => ApiActions.filteredEmployeesLoaded({ filteredData: data })));
            })
        )
    });


    loadEmployeesTotal$ = createEffect(() => {
        return this.actions.pipe(
            ofType(PageActions.enter),
            exhaustMap(() => {
                return this.employeesService.loadEmployeesTotalNumber()
                    .pipe(map(total => ApiActions.totalNumberLoaded({ total })))
            })
        )
    });


    loadDepartments$ = createEffect(() => {
        return this.actions.pipe(
            ofType(PageActions.loadDepartments),
            exhaustMap(() => {
                return this.employeesService.loadDepartments()
                    .pipe(map(data => ApiActions.departmentsLoaded({ departments: data })));
            })
        )
    });


    loadCities$ = createEffect(() => {
        return this.actions.pipe(
            ofType(PageActions.loadCities),
            exhaustMap(() => {
                return this.employeesService.loadCities()
                    .pipe(map(data => ApiActions.citiesLoaded({ cities: data })));
            })
        )
    })
}