import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Employee } from "./models/employee";
import { FilteredEmployees } from "./models/filteredEmployees";
import { FiltersDto } from "./models/filtersDto";

const API_URL = "http://localhost:3000/api/employees";


@Injectable({
    providedIn: "root"
})
export class EmployeesService {
    constructor(private http: HttpClient) { }


    loadFilteredEmployees(filter: FiltersDto): Observable<FilteredEmployees> {

        let url = API_URL + `?page=${filter.page}&limit=${filter.limit}`;
        if (filter.department) { url += `&department=${filter.department}` };
        if (filter.city) { url += `&city=${filter.city}` };

        return this.http.get<FilteredEmployees>(url);
    }


    loadEmployeesTotalNumber(): Observable<number> {
        return this.http.get<number>(API_URL + "/total");
    }


    loadEmployeeById(id: number): Observable<Employee> {
        return this.http.get<Employee>(`${API_URL}/${id}`);
    }


    loadEmployeeByUserId(userId: number): Observable<Employee> {
        return this.http.get<Employee>(`${API_URL}/user/${userId}`);
    }


    addEmployee(employeeData: Partial<Employee>): Observable<Employee> {
        return this.http.post<Employee>(API_URL, employeeData);
    }


    updateEmployee(updateData: Partial<Employee>, existingId: number): Observable<Employee> {
        return this.http.put<Employee>(`${API_URL}/${existingId}`, updateData);
    }


    deleteEmployee(id: number): Observable<any> {
        return this.http.delete<any>(API_URL + '/' + id);
    }


    loadDepartments(): Observable<string[]> {
        return this.http.get<any>(API_URL + "/departments");
    }

    loadCities(): Observable<string[]> {
        return this.http.get<any>(API_URL + "/cities");
    }
}