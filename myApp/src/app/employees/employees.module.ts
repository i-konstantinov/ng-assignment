import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorModule } from '@angular/material/paginator';

import { EmployeesListComponent } from './components/employees-list/employees-list.component';
import { AddEmployeeComponent } from './components/add-employee/add-employee.component';
import { EmployeeModalComponent } from './components/employee-modal/employee-modal.component';

import { NameValidatorDirective } from '../core/validators/name-validator.directive';
import { TranslateModule } from '@ngx-translate/core';
import { EmployeesStateModule } from './employees.state';

import { EffectsModule } from '@ngrx/effects'
import { EmployeesListEffects } from './components/employees-list/effects/employees-list.effects';
import { AddEmployeeApiEffects } from './components/add-employee/effects/add-employee.effects';
import { EmployeeModalEffects } from './components/employee-modal/effects/employee-modal.effects';




@NgModule({
  declarations: [
    NameValidatorDirective,
    AddEmployeeComponent,
    EmployeesListComponent,
    EmployeeModalComponent
  ],
  imports: [
    CommonModule,
    EmployeesStateModule,
    EffectsModule.forFeature([
      EmployeesListEffects,
      AddEmployeeApiEffects,
      EmployeeModalEffects
    ]),
    ReactiveFormsModule,
    MatDialogModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    MatPaginatorModule,
    RouterModule,
    TranslateModule,
  ],
  exports: [
    AddEmployeeComponent,
    EmployeesListComponent,
    EmployeeModalComponent
  ]
})
export class EmployeesModule { }
