import { createReducer, on, createSelector } from "@ngrx/store";
import { Employee } from "./models/employee";
import { PaginatorMetaData } from "./models/paginatorMetaData";

import * as ngrxHelpers from "./helpers/ngrxHelpers";

import { viewProfile as ViewProfileFromHeader } from "../core/components/header/actions/header.component.actions";

import { employeeAdded as NewEmployeeFromApi } from "./components/add-employee/actions/add-employee-api.actions";

import * as EmployeesListPageActions from './components/employees-list/actions/employees-list-page.actions';
import * as EmployeesListApiActions from "./components/employees-list/actions/employees-list-api.actions";

import * as EmployeeModalPageActions from "./components/employee-modal/actions/employee-modal-page.actions";
import * as EmployeeModalApiActions from "./components/employee-modal/actions/employee-modal-api.actions";


export interface State {
    collection: Employee[],
    activeEmployee: Employee | null,
    totalEmployees: number
    employeesDepartments: string[],
    employeesCities: string[],
    paginatorData: PaginatorMetaData | null
}

export const initialState: State = {
    collection: [],
    activeEmployee: null,
    totalEmployees: 0,
    employeesDepartments: [],
    employeesCities: [],
    paginatorData: null
}


export const reducer = createReducer(

    initialState,

    // Employees List
    on(EmployeesListApiActions.totalNumberLoaded, (state, action) => {
        return {
            ...state,
            totalEmployees: action.total
        }
    }),
    on(EmployeesListApiActions.filteredEmployeesLoaded, (state, action) => {
        return {
            ...state,
            collection: action.filteredData.items,
            paginatorData: action.filteredData.meta
        }
    }),
    on(EmployeesListPageActions.openDetails, (state, action) => {
        return {
            ...state,
            activeEmployee: action.employee
        }
    }),
    on(EmployeesListApiActions.citiesLoaded, (state, action) => {
        return {
            ...state,
            employeesCities: action.cities
        }
    }),
    on(EmployeesListApiActions.departmentsLoaded, (state, action) => {
        return {
            ...state,
            employeesDepartments: action.departments
        }
    }),

    // Add Employee
    on(NewEmployeeFromApi, (state, action) => {
        return {
            ...state,
            collection: ngrxHelpers.addEmployee(state.collection, action.employee)
        }
    }),

    // Employee Modal
    on(EmployeeModalApiActions.employeeUpdated, (state, action) => {
        return {
            ...state,
            collection: ngrxHelpers.updateEmployee(state.collection, action.employee),
            activeEmployee: action.employee
        }
    }),
    on(EmployeeModalApiActions.employeeDeleted, (state, action) => {
        return {
            ...state,
            collection: ngrxHelpers.deleteEmployee(state.collection, action.employeeId),
            activeEmployee: null
        }
    }),
    on(EmployeeModalPageActions.exit, (state) => {
        return {
            ...state,
            activeEmployee: null
        }
    }),

    // Header
    on(ViewProfileFromHeader, (state, action) => {
        return {
            ...state,
            activeEmployee: action.employee
        }
    })
);


export const selectAll = (state: State) => state;
/**
 * * Create Selector *
 * - all args without last one are all the selectors that we want to consume
 * - the last arg is a callback func, that takes the results of the provided selectors
 * - we use the callback func to figure out the piece of state that we're trying to create;
 * - createSelector, will invoke the callback ONLY whenever the value of some of the provided selectors change
 * in the meantime, selectors will retain whatever state value we've produced;
 */
export const selectAllEmployees = createSelector(selectAll, (state) => {
    return state.collection;
});

export const selectTotalEmployees = createSelector(selectAll, (state) => {
    return state.totalEmployees;
});

export const selectActiveEmployee = createSelector(selectAll, (state) => {
    return state.activeEmployee;
});

export const selectPaginatorData = createSelector(selectAll, (state) => {
    return state.paginatorData;
});

export const selectDepartments = createSelector(selectAll, (state) => {
    return state.employeesDepartments;
});

export const selectCities = createSelector(selectAll, (state) => {
    return state.employeesCities;
});