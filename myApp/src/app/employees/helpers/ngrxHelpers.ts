import { Employee } from "../models/employee";

export const addEmployee = (
    employees: Employee[],
    newEmployee: Employee
) => [...employees, newEmployee];

export const updateEmployee = (
    employees: Employee[],
    updated: Employee
) => employees.map(record => {
    return record.id === updated.id
    ? Object.assign({}, record, updated)
    : record;
});

export const deleteEmployee = (
    employees: Employee[],
    employeeId: number
) => employees.filter(record => record.id !== employeeId);