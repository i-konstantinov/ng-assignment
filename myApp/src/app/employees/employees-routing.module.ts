import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppAuthGuard } from '../auth/guards/appAuthguard';
import { AddEmployeeComponent } from './components/add-employee/add-employee.component'; 
import { EmployeesListComponent } from './components/employees-list/employees-list.component'; 

const routes: Routes = [
  {
    path: "employees",
    component: EmployeesListComponent,
    canActivate: [ AppAuthGuard ]
  },
  {
    path: "employees/add",
    component: AddEmployeeComponent,
    canActivate: [ AppAuthGuard ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class EmployeesRoutingModule { }
