
export interface Employee {
    id: number,
    userId: number,

    firstName: string,
    lastName: string,
    position: string,
    email: string,
    salary: number
    
    department ?: string,
    city ?: string,
    status ?: string,
}