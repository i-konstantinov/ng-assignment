import { Employee } from "./employee";
import { PaginatorMetaData } from "./paginatorMetaData";

export interface FilteredEmployees {
    items: Employee[],
    meta: PaginatorMetaData
}