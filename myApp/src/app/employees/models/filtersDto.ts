export interface FiltersDto {
    page: number;
    limit: number;
    department ?: string;
    city ?: string;
}