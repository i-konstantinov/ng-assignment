export interface PaginatorMetaData {
    totalItems: number;
    itemCount: number;
    totalPages: number;
    currentPage: number;
}