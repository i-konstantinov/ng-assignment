
export interface EmployeeDto {
    firstName: string,
    lastName: string,
    email: string,
    position: string,
    salary: number,

    department ?: string,
    city ?: string,
    status ?: string,
    
    userId: number
}