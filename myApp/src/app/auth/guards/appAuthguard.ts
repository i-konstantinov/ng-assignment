import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { Subscription } from "rxjs";
import { selectUser } from "../auth.state";
import { User } from "../models/userDto";

@Injectable()
export class AppAuthGuard implements CanActivate {
    user!: User | null;
    subscription!: Subscription;

    constructor(
        private store: Store,
        private router: Router
    ) {
        this.subscription = this.store.select(selectUser).subscribe(
            user => this.user = user
        );
    }
    
    canActivate() {
        this.subscription.unsubscribe();

        if (this.user) return true;
        else return this.router.navigate([ "/login" ]);
    }
}