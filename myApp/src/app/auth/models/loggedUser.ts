import { TokensDto } from "./tokensDto";
import { User } from "./userDto";

export interface LoggedUser {
    user: User,
    tokens: TokensDto
}