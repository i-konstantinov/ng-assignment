import { Observable, tap } from 'rxjs';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserCredentials } from '../models/userCredentials';
import { LoggedUser } from '../models/loggedUser';
import { TokensDto } from '../models/tokensDto';
import { User } from '../models/userDto';


const API_URL = "http://localhost:3000/api/auth";


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }


  register(user: UserCredentials): Observable<LoggedUser> {
    return this.http.post<LoggedUser>(API_URL, user)
      .pipe(
        tap(
          (data: LoggedUser) => {
            this.saveTokens(data.tokens);
            this.saveUser(data.user);
            this.router.navigate(['/employees']);
          }
        )
      );
  }


  login(user: UserCredentials): Observable<LoggedUser> {
    return this.http.get<LoggedUser>(API_URL + `?username=${user.username}&password=${user.password}`)
      .pipe(
        tap(
          (data: LoggedUser) => {
            this.saveTokens(data.tokens);
            this.saveUser(data.user);
            this.router.navigate(['/employees']);
          }
        )
      );
  }


  logout(): void {
    localStorage.clear();
    this.router.navigate(['/']);
  }


  refresh(): Observable<any> {
    return this.http.post<any>(API_URL + '/refresh', { refreshToken: this.getTokens()?.refreshToken })
      .pipe(tap(tokens => this.saveTokens(tokens)));
  }


  saveTokens(tokens: TokensDto) {
    localStorage.setItem("accessToken", tokens.accessToken);
    localStorage.setItem("refreshToken", tokens.refreshToken);
  }

  getTokens(): TokensDto | null {
    const accessToken = localStorage.getItem("accessToken");
    const refreshToken = localStorage.getItem("refreshToken");
    if (accessToken != null && refreshToken != null) return { accessToken, refreshToken };
    else return null;
  }

  saveUser(user: User) {
    localStorage.setItem("userId", user.id.toString());
    localStorage.setItem("username", user.username);
  }

  getUser(): User | null {
    const id = localStorage.getItem("userId");
    const username = localStorage.getItem("username");
    if (id && username) return { id: Number(id), username };
    else return null;
  }
}
