import { createReducer, on, createSelector } from "@ngrx/store";
import { User } from "./models/userDto";

import * as HeaderActions from "../core/components/header/actions/header.component.actions";
import * as AuthApiActions from "./actions/auth-api.actions";
import { employeeAdded as ApiNewEmployee } from "../employees/components/add-employee/actions/add-employee-api.actions";
import { employeeUpdated as ApiUpdatedEmployee, employeeDeleted as ApiDeletedEmployee } from "../employees/components/employee-modal/actions/employee-modal-api.actions";
import { Employee } from "../employees/models/employee";


const id: number | null = localStorage.getItem("userId") ? Number(localStorage.getItem("userId")) : null;
const username: string | null = localStorage.getItem("username");
let userData = null;
if ((id && typeof id == "number") && (username != null)) { userData = { id, username }; };



export interface State {
    user: User | null;
    employeeProfile: Employee | null;
}

export const initialState: State = {
    user: userData,
    employeeProfile: null
}


export const authReducer = createReducer(
    initialState,
    on(
        AuthApiActions.userLogged,
        AuthApiActions.userRegisteredAndLogged,
        (state, action) => {
            return {
                ...state,
                user: action.user
            }
        }
    ),
    on(
        AuthApiActions.userProfileLoadedSuccess,
        ApiNewEmployee,
        ApiUpdatedEmployee,
        (state, action) => {
        return {
            ...state,
            employeeProfile: action.employee
        }
    }),
    on(
        AuthApiActions.userProfileLoadedFail,
        ApiDeletedEmployee,
        (state) => {
        return {
            ...state,
            employeeProfile: null
        }
    }),
    on(HeaderActions.logOut, (state) => {
        return {
            ...state,
            user: null,
            employeeProfile: null
        }
    })
);


export const selectState = (state: State) => state;

export const selectUser = createSelector(selectState, (state) => {
    return state.user;
});

export const selectUserId = createSelector(selectUser, (user) => {
    return user?.id;
});

export const selectUserUsername = createSelector(selectUser, (user) => {
    return user?.username;
});

export const selectUserProfile = createSelector(selectState, (state) => {
    return state.employeeProfile;
});