import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, concatMap, switchMap } from "rxjs";
import { AuthService } from "../service/auth.service";
import { EmployeesService } from "src/app/employees/employees.service";

import * as AuthPageActions from "../actions/auth-page.actions";
import * as AuthApiActions from "../actions/auth-api.actions";
import * as HeaderActions from "../../core/components/header/actions/header.component.actions";


@Injectable()
export class AuthEffects {
    constructor(
        private actions: Actions,
        private authService: AuthService,
        private employeesService: EmployeesService
    ) { }

    login$ = createEffect(() => {
        return this.actions.pipe(
            ofType(AuthPageActions.submitLogin),
            concatMap((data) => {
                return this.authService.login(data.userCredentials)
                    .pipe(map((data) => AuthApiActions.userLogged({ user: data.user })))
            })
        )
    });

    register$ = createEffect(() => {
        return this.actions.pipe(
            ofType(AuthPageActions.submitRegistration),
            concatMap((data) => {
                return this.authService.register(data.userCredentials)
                    .pipe(map((data) => AuthApiActions.userRegisteredAndLogged({ user: data.user })))
            })
        )
    });

    getUsersEmployeeProfile$ = createEffect(() => {
        return this.actions.pipe(
            ofType(
                AuthApiActions.userLogged,
                AuthApiActions.userRegisteredAndLogged,
                HeaderActions.getProfile
                ),
            switchMap((userData) => {
                return this.employeesService.loadEmployeeByUserId(userData.user.id)
                .pipe(map((employee) => {
                    if (employee) {
                        return AuthApiActions.userProfileLoadedSuccess({ employee: employee });
                    } else {
                        return AuthApiActions.userProfileLoadedFail();
                    }
                }))
            })
        )
    });
}
