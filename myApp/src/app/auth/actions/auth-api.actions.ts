import { createAction, props } from "@ngrx/store";
import { Employee } from "src/app/employees/models/employee";
import { User } from "../models/userDto";


export const userLogged = createAction(
    "[Auth Component] User Logged",
    props<{ user: User }>()
);
export const userLoggedFail = createAction(
    "[Auth Component] Login Failed"
);

export const userRegisteredAndLogged = createAction(
    "[Auth Component] User Registered And Logged",
    props<{ user: User }>()
);
export const userRegistrationFail = createAction(
    "[Auth Component] Registration Failed"
);

export const userProfileLoadedSuccess = createAction(
    "[Auth Component] User's Employee Profile Found",
    props<{ employee: Employee }>()
);

export const userProfileLoadedFail = createAction(
    "[Auth Component] Employee Profile Not Found"
);