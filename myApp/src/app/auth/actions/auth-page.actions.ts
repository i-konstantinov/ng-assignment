import { createAction, props } from '@ngrx/store';
import { UserCredentials } from '../models/userCredentials';


export const enterRegister = createAction(
    "[Auth Component] Enter Register"
);
export const submitRegistration = createAction(
    "[Auth Component] Submit Register Form",
    props<{ userCredentials: UserCredentials }>()
);


export const enterLogin = createAction(
    "[Auth Component] Enter Login"
);
export const submitLogin = createAction(
    "[Auth Component] Submit Login Form",
    props<{ userCredentials: UserCredentials }>()
);
