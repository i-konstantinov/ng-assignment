import { NgModule } from "@angular/core";
import { ActionReducerMap, createFeatureSelector, createSelector, MetaReducer, StoreModule } from "@ngrx/store";

import * as fromAuth from "./auth.reducer";

export const FEATURE_KEY = "auth";


/**
 * The shape of the State
 */
export interface State {
    user: fromAuth.State
};

export const reducers: ActionReducerMap<State> = {
    user: fromAuth.authReducer
};

export const metaReducers: MetaReducer<State>[] = [];

/**
 * Module
 */
@NgModule({
    imports: [StoreModule.forFeature(FEATURE_KEY, reducers, { metaReducers })],
})
export class AuthStateModule {}


/**
 * Feature Selector
 */
export const selectAuthFeatureState = createFeatureSelector<State>(FEATURE_KEY);



export const selectAuthState = createSelector(
    selectAuthFeatureState,
    (stateData) => stateData.user
);

export const selectUser = createSelector(
    selectAuthState,
    fromAuth.selectUser
);

export const selectUserId = createSelector(
    selectAuthState,
    fromAuth.selectUserId
);

export const selectUserUsername = createSelector(
    selectAuthState,
    fromAuth.selectUserUsername
);

export const selectUsersEmployeeProfile = createSelector(
    selectAuthState,
    fromAuth.selectUserProfile
);