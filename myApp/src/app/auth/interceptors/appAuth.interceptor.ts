import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { BehaviorSubject, catchError, filter, Observable, switchMap, throwError, take } from "rxjs";
import { AuthService } from "../service/auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private isRefreshing = false;
    private refreshedTokens$: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(private readonly authService: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const accessToken = this.authService.getTokens()?.accessToken;

        if (accessToken) {
            req = this.attachAccessToken(req, accessToken);
        }
        return next.handle(req).pipe(catchError(err => {
            const message = "Caught in interceptor: ";
            console.log(message, err);
            if (err.status === 401 && err.statusText === "Unauthorized") {
                return this.handle_401(req, next);
            }
            else {
                return throwError(err);
            }
        }));
    }


    private attachAccessToken(req: HttpRequest<any>, token: string) {
        return req.clone({
            headers: req.headers.set("my-access-token", token)
        });
    }


    private handle_401(req: HttpRequest<any>, next: HttpHandler) {
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshedTokens$.next(null);

            return this.authService.refresh().pipe(
                switchMap(tokens => {
                    this.isRefreshing = false;
                    this.refreshedTokens$.next(tokens.refreshToken);
                    return next.handle(this.attachAccessToken(req, tokens.accessToken));
                }));
        } else {
            return this.refreshedTokens$.pipe(
                filter(data => data != null),
                take(1),
                switchMap(tokens => {
                    return next.handle(this.attachAccessToken(req, tokens.accessToken));
                })
            )
        }
    }
}
