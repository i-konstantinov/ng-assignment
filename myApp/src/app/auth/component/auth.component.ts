import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PasswordsValidator } from 'src/app/core/validators/passwords-compare.directive';
import { UserCredentials } from '../models/userCredentials';
import { Store } from '@ngrx/store';

import * as AuthPageActions from "../actions/auth-page.actions";




@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  showRegister!: boolean;
  showLogin!: boolean;
  
  registerForm!: FormGroup<any>;
  loginForm!: FormGroup<any>;

  constructor(
    private store: Store,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) { }


  ngOnInit(): void {
    const link = this.activatedRoute.snapshot.url.toString();
    if (link === "register") {
      this.store.dispatch(AuthPageActions.enterRegister());
      this.showRegister = true;
      this.showLogin = false;
    };
    if (link === "login") {
      this.store.dispatch(AuthPageActions.enterLogin());
      this.showRegister = false;
      this.showLogin = true;
    }

    this.registerForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      rePass: [null, [Validators.required]]
    }, {
      updateOn: 'blur',
      validators: [PasswordsValidator.compare]
    });

    this.loginForm = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  onRegister(): void {
    if (this.registerForm.invalid) { return };

    const userCreds: UserCredentials = {
      username: this.registerForm.get("username")?.value,
      password: this.registerForm.get("password")?.value
    }

    this.store.dispatch(AuthPageActions.submitRegistration({ userCredentials: userCreds }));
  }

  onLogin(): void {
    if (this.loginForm.invalid) { return };

    const userCreds: UserCredentials = {
      username: this.loginForm.get("username")?.value,
      password: this.loginForm.get("password")?.value
    }

    this.store.dispatch(AuthPageActions.submitLogin({ userCredentials: userCreds }));
  }
}
